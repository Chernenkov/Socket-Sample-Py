import socket
import requests


def get_local_network_info():
    try:
        print("\nLocal info:")
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(("192.168.0.1", 80))
        hostname = socket.gethostname()
        loop_ip = socket.gethostbyname(socket.gethostname())
        local_ip = s.getsockname()[0]
        s.close()
        print("Local hostname: ", hostname)
        print("Loop IP: ", loop_ip)
        print("LAN IP: ", local_ip)
    except OSError:
        print("An error occurred while requesting local network info. Check local network connection")


def get_public_info():
    try:
        print("\nPublic info:")
        response = requests.get('https://api.myip.com')
        resp_json = response.json()
        public_ip = resp_json['ip']
        country_name = resp_json['country']
        country_code = resp_json['cc']
        print("Public IP address: ", public_ip)
        print("Country: ", country_name)
        print("Country code: ", country_code)
    except requests.exceptions.ConnectionError:
        print("An error occurred while requesting public IPv4 address. Check Internet connection.")
